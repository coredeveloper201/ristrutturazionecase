import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'
import Meta from 'vue-meta';

Vue.use(Meta);
Vue.use(Vuex);
Vue.use(VueAxios, axios);

// Vue.use(require('vue-resource'));

export const store = new Vuex.Store({
    state : {
        lang : 'it',
        auth : {},
        profiles: [],
        projects: []
    },
    getters: {
        profiles(state) {
            return state.profiles;
        },

        projects(state) {
            return state.projects;
        }
	},
	mutations: {
		updateProfiles(state, payload) {
            state.profiles = payload;
        },
        updateProjects(state, payload) {
            state.projects = payload;
        }
	},
	actions: {
        getProfiles(context) {
            axios.get('https://dev2.simonechinaglia.net/dev/ristru-backend/api/profiles')
            .then((response) => {
                context.commit('updateProfiles', response.data.profiles);
            })
        },
        getProjects(context) {
            axios.get('https://dev2.simonechinaglia.net/dev/ristru-backend/api/projects')
            .then((response) => {
                context.commit('updateProjects', response.data.projects);
            })
        }
	}
});
