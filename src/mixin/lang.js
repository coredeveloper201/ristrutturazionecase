let Language = {
    __: function (str) {
        let d = [
            // Top Bar
            {it: 'DOVE SIAMO', en:'WHERE WE ARE', de:'', py:''},
            {it: 'Simone in tutta Italia', en:'Find your near Office !', de:'', py:''},
            {it: 'Chiamaci adesso!', en:'CALL US NOW', de:'', py:''},
            {it: "Mandaci un'email", en:'WRITE US AN EMAIL', de:'', py:''},
            {it: 'AMBIENTI', en:'INTERIOR DESIGN', de:'', py:''},
            {it: 'AMBIENTI', en:'INTERIOR DESIGN', de:'', py:''},
            {it: 'I NOSTRI PROGETTI', en:'OUR PROJECTS', de:'', py:''},
            {it: 'CHI SIAMO', en:'WHO WE ARE', de:'', py:''},
            {it: 'CONTATTACI', en:'CONTACT US', de:'', py:''},
            {it: 'IL TUO ARCHITETTO', en:'YOUR ARCHITECT', de:'', py:''},
            {it: 'ILTEAM', en:'', de:'', py:''},
            {it: 'L’AZIENDA', en:'', de:'', py:''},

            // Banner
            {it: 'SIAMO LEADER IN ITALIA', en:'WE ARE LEADER IN ITALY', de:'', py:''},
            {it: 'Ristruttura la tua casa', en:'Renew your House', de:'', py:''},
            {it: 'con noi!', en:'with us!', de:'', py:''},
            {it: 'Cosa facciamo', en:'WHAT WE DO', de:'', py:''},
            {it: 'CUCINA', en:'', de:'', py:''},

            // form 1
            {it: 'Inizia oggi la tua ristrutturazione', en:'Start today your renovation', de:'', py:''},
            {it: 'Scrivi il tuo nome', en:'Type here your name', de:'', py:''},
            {it: 'la tua email', en:'Your email', de:'', py:''},
            {it: 'Cellulare', en:'Mobile phone', de:'', py:''},
            {it: 'Cosa vuoi ristrutturare', en:'Choose the room', de:'', py:''},
            {it: 'Inviando il form autorizzi Ristrutturazione case a contattarti per fini commerciali. Dichiari inoltre di aver letto e di accettare la Privacy Policy.', en:'By submitting this form you authorize Ristrutturazione Case to contacts for commercial purposes. You also declare that you have read and accept the Privacy Policy.', de:'', py:''},
            {it: 'Completa la richiesta', en:'Complete the request', de:'', py:''},
            {it: 'PREVENTIVO GRATUITO', en:'FREE QUOTE', de:'', py:''},

            // Restructure
            {it: 'LA RISTRUTTURAZIONE CHE LA TUA CASA VUOLE', en:'THE RENOVATION THAT YOUR HOUSE WANTS.', de:'', py:''},
            {it: 'Ristruttura oggi la tua casa!', en:'Renew today you House!', de:'', py:''},
            {it: 'Progetto virtuale VR', en:'A VR dedicated project', de:'', py:''},
            {it: 'Un progetto in realtà virtuale cosi da avere la certezza di come sarà l’appartamento ristrutturato ancor prima di iniziare i lavori.', en:'A virtual reality project so as to be sure of how the apartment will be renovated before starting work.', de:'', py:''},
            {it: 'Un unico Architetto', en:'One architect for you', de:'', py:''},
            {it: 'unico Architetto', en:'Architect for you', de:'', py:''},
            {it: 'Uno dei nostri architetti come unico referente dei lavori, che segue il processo di ristrutturazione dal progetto alla consegna chiavi in mano.', en:'One of our architects as the sole referent of the works, which follows the restructuring process from the project to turnkey delivery.', de:'', py:''},
            {it: 'Appartamento gratuito', en:'Free flat', de:'', py:''},
            {it: 'Somontaggio mobili', en:'Furniture replacement', de:'', py:''},
            {it: 'Smontaggio, fermo deposito e rimontaggio dei tuoi mobili a fine ristrutturazione; il tutto a costo zero. Un servizio efficiente con tempi brevi e certi.', en:'Dismantling, storage and reassembly of your furniture at the end of the renovation; all at no cost. An efficient service with short and certain times.', de:'', py:''},
            {it: 'Applicazione dedicata', en:'Mobile application', de:'', py:''},
            {it: 'Controlla l’avanzamento dei lavori grazie ad un’applicazione dedicata e scopri come stiamo effettuando la tua ristrutturazione.', en:'Check the progress of the work thanks to a dedicated application and find out how we are doing your renovation.', de:'', py:''},
            {it: 'Rispetto dei tempi', en:'Respected dedaline', de:'', py:''},
            {it: 'Rispetto dei tempi', en:'Respected dedaline', de:'', py:''},
            {it: 'Uno dei nostri architetti come unico referente dei lavori, che segue il processo di ristrutturazione dal progetto alla consegna chiavi iRispetto dei tempi di consegna della tua casa ristrutturata. Intraprendi la tua ristrutturazione con tranquillità, garantiamo tempi certi di consegna.', en:'One of our architects as the sole referent of the works, which follows the restructuring process from the project to the delivery of the keys in respect of the delivery times of your renovated home. Undertake your restructuring with peace of mind, we guarantee certain delivery times.', de:'', py:''},
            {it: 'Progetto virtuale VR', en:'A VR dedicated project', de:'', py:''},
            {it: 'Progetto virtuale', en:'A dedicated project', de:'', py:''},

            // Renovations
            {it: 'PROGETTI RECENTI', en:'RECENT RENOVATION PROJECTS', de:'', py:''},
            {it: 'Nuovi spazi da scroprire', en:'New space to discover', de:'', py:''},

            // Quote
            {it: 'Richiedi un preventivo gratuito', en:'Request a free quote!', de:'', py:''},
            {it: 'CHIAMACI AL 800 33 33 30', en:'CALL US 800 33 33 30', de:'', py:''},
            {it: 'CHIAMACI AL 800 33 33 30', en:'CALL US 800 33 33 30', de:'', py:''},
            {it: 'PRENOTA UN APPUNTAMENTO', en:'BOOK TODAY YOUR APPOINTMENT', de:'', py:''},
            {it: 'Matteo di Maglie', en:'', de:'', py:''},

            // Footer
            {it: 'Menu veloce', en:'Quick menu', de:'', py:''},
            {it: 'Homepage', en:'Homepage', de:'', py:''},
            {it: 'Preventivo gratuito', en:'Free quote', de:'', py:''},
            {it: 'Contattaci', en:'Contact us', de:'', py:''},
            {it: 'Siamo in tutta Italia', en:'We cover all Itlay', de:'', py:''},
            {it: 'Tutti i testi e la grafica presenti nel sito sono soggetti alle norme vigenti sul diritto d autore.', en:'All texts and graphics on this site are subject to the current copyright regulations.', de:'', py:''},
            {it: 'Segnala un problema', en:'Feedback a problem', de:'', py:''},

            // banner all
            {it: 'RISTRUTTURAZIONE CHIAVI IN MANO', en:'KEY RESTRUCTURING IN HAND', de:'', py:''},
            {it: 'Ristrutturazione Cucine', en:'Kitchen Renovation', de:'', py:''},
            {it: 'CUCINA', en:'KITCHEN', de:'', py:''},

            // Architect Renovations
            {it: 'Responsabile produzione', en:'Production manager', de:'', py:''},
            {it: 'Direzione', en:'Direction', de:'', py:''},
            {it: 'Amministrazione', en:'Administration', de:'', py:''},
            {it: 'Architetti', en:'Architects', de:'', py:''},
            

            // single Architect
            {it: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed scelerisque libero sed tincidunt elementum. Praesent elementum, risus nec dictum consectetur, turpis enim semper odio, ut efficitur tellus justo nec odio. Integer interdum justo at ante lacinia, a molestie ex eleifend. Maecenas pharetra convallis elementum. Praesent pharetra ante vitae est pharetra, id aliquam sem tempus. Mauris et nisi porta, hendrerit purus vel, ullamcorper enim. Nulla quis urna ut nisi vehicula fringilla at finibus urna. Nunc convallis mi eu porttitor tristique.', en: '', de:'', py:''},
            {it: 'Cras placerat sit amet ex quis cursus. Sed at est sit amet urna condimentum iaculis. Mauris et purus ut massa vehicula convallis vitae pharetra magna.', en: '', de:'', py:''},
            {it: 'LA MIA ESPERIENZA', en:'', de:'', py:''},
            {it: 'I MIEI PUNTI FORTI', en:'', de:'', py:''},
            {it: 'Ristrutturazione Case', en:'Ristrutturazione Case', de:'', py:''},
            {it: 'Dal 2013 ad oggi', en:'', de:'', py:''},
            {it: 'Società xyz', en:'', de:'', py:''},
            {it: 'Dal', en:'', de:'', py:''},
            {it: 'al', en:'', de:'', py:''},
            {it: 'Politecnico di Milano', en:'', de:'', py:''},

            // Ambienti
            {it: 'SONO ESPERTO IN ARREDAMENTO CONTEMPORANEO', en:'', de:'', py:''},
            {it: 'Scegli il tuo ambiente', en:'', de:'', py:''},
            {it: 'APPARTAMENTO', en:'FLAT', de:'', py:''},
            {it: 'NEGOZIO', en:'SHOP', de:'', py:''},
            {it: 'UFFICIO', en:'OFFICE', de:'', py:''},
            {it: 'Un progetto in realtà', en:'A project in reality', de:'', py:''},
            {it: 'virtuale cosi da avere', en:'virtual so as to be', de:'', py:''},
            {it: 'LAVORI DI RISTUTTURAZIONE RECENTI', en:'', de:'', py:''},
            {it: 'Ambienti nuovi da scoprire', en:'', de:'', py:''},
            {it: 'SOGGIORNO', en:'', de:'', py:''},
            {it: 'Ristruttrazione appartamento 90mq', en:'', de:'', py:''},
            {it: 'Scopri di più', en:'', de:'', py:''},
            {it: 'SOGGIORNO', en:'', de:'', py:''},

            // portfolio
            {it: 'Le nostre ristrutturazioni', en:'', de:'', py:''},
            {it: 'Cucina shabby shic', en:'', de:'', py:''},
            {it: 'SCOPRI DI PIU', en:'FIND OUT MORE', de:'', py:''},
            
            // single portfolio
            {it: 'Vivamus tempus risus metus, sed porttitor nibh auctor et. Nunc hendrerit diam sed turpis imperdiet, ut lobortis dolor bibendum. Sed gravida pretium leo in suscipit. Vestibulum pharetra tortor tellus, ut mattis magna fringilla sit amet. Fusce iaculis nisi quis ex dapibus, non pellentesque ipsum pulvinar. Suspendisse euismod quis erat a lobortis. Sed pellentesque pellentesque erat vitae luctus. Cras quis est enim. Maecenas quam lacus, eleifend vel leo eget, malesuada porta lorem. Nam lectus libero, congue id facilisis et, molestie id risus. Morbi volutpat, odio nec commodo egestas, risus ligula viverra ante, ac dictum eros ante in augue. Suspendisse justo massa, maximus nec erat a, ultricies tincidunt ante.', en:'', de:'', py:''},
            {it: 'Ut vehicula consequat nulla, sit amet aliquam lorem luctus et. Phasellus consectetur congue lacinia. Morbi tincidunt nunc ut libero aliquam, eget accumsan ante rutrum. Donec auctor faucibus leo, eu venenatis leo aliquam in. Sed at tellus in leo porttitor tempor vitae sagittis lacus. Sed a aliquam massa. Morbi sed dui eu ante commodo finibus vel id ante. Mauris vitae odio libero. Praesent pellentesque, velit sed elementum porta, neque orci mattis augue, sed mattis arcu turpis a ipsum. Quisque condimentum, neque eu sagittis sodales, sapien lorem iaculis dolor, sed rutrum nibh sem convallis orci.', en:'', de:'', py:''},
            {it: 'SMONTAGGIO MOBILI', en:'', de:'', py:''},
            {it: 'COMPILA IL FORM', en:'', de:'', py:''},
            {it: 'Cliente Privato', en:'', de:'', py:''},
            {it: 'Tipologia', en:'Type', de:'', py:''},
            {it: 'Data', en:'Date', de:'', py:''},
            {it: 'Budget', en:'Budget', de:'', py:''},
            {it: 'Città', en:'City', de:'', py:''},
            {it: 'Superficie', en:'Surface', de:'', py:''},
            {it: 'Consegna', en:'Delivery', de:'', py:''},
            {it: 'Condividi', en:'Share', de:'', py:''},
            {it: '12 giorni', en:'12 days', de:'', py:''},
            

            // contact
            {it: 'Siamo in tutta italia!', en:'', de:'', py:''},
            {it: 'Via Petrocchi', en:'', de:'', py:''},
            {it: 'Milano', en:'', de:'', py:''},
            {it: 'Passo successivo', en:'', de:'', py:''},

            // about 
            {it: 'Chiedici un preventivo gratuito!', en: 'Ask us for a free quote!', de:'', py:''},
            {it: 'PRENOTA OGGI UN APPUNTAMENTO', en: '', de:'', py:''},
            {it: 'commerciali. Dichiari inoltre di aver letto e di accettare la Privacy Policy.', en: '', de:'', py:''},
            {it: 'Inviando questo form autorizzi Ristrutturazione Case a contatti per fini', en: '', de:'', py:''},
            {it: 'grazie. Adesso le richiedo informazioni più dettagliete sulla ristrutturazione.', en: '', de:'', py:''},
            {it: 'L’appuntamento telefonico ha scopo di analizzare il progetto, la fattibilità, costi e tempistiche generali non vincolanti.', en: '', de:'', py:''},
            {it: 'Desidero il sopralluogo', en: '', de:'', py:''},
            {it: 'Un Architetto incaricato verrà a', en: '', de:'', py:''},
            {it: 'isionare l’appartamento o', en: '', de:'', py:''},
            {it: 'il negozio', en: '', de:'', py:''},
            {it: 'Se si, indicami la città', en: '', de:'', py:''},
            {it: 'grazie. Abbiamo preso in carico la tua richiesta di preventivo. Un incaricato si metterà in contatto con te. Abbiamo anche provveduto ad invitarti una copia della richiesta sulla tua casella email.', en: '', de:'', py:''},
            {it: 'Preventivo numero:', en: '', de:'', py:''},
            {it: 'Preventivo numero:', en: '', de:'', py:''},


        ];

        if(localStorage.lang !== undefined){
            this.$store.state.lang = localStorage.lang;
        }
        else {
            localStorage.lang = this.$store.state.lang;
        }
        let lang = this.$store.state.lang;
        let index = d.findIndex((item) => item.it.toLowerCase() === str.toLowerCase());
        if(index > -1){
            if(d[index][lang] !== undefined){
                return d[index][lang];
            }
            else {
                return str;
            }
        } else {
            return str;
        }
    }
};

module.exports = Language;