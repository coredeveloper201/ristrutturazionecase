<?php
    $url = $_SERVER['REQUEST_URI'];
    if($url == '/'):
        $keyword = "ristrutturazione, ristrutturazioni, detrazioni fiscali, ristrutturazione case, preventivo ristrutturazione, costo ristrutturazione mq";
        $description = "Ristruttura oggi il tuo appartamento. Ristrutturazioni complete da 330€/mq. Preventivo, sopralluogo e progetto Gratuito ";
        $title = "Ristrutturazione appartamenti, case e uffici - Preventivo Gratuito";
        $image = "";
    else if(strpos($url, '/architect/')):
        $keyword = "";
        $description = "";
        $title = "Ristrutturazione Case";
        $author = "";
        $image = "";
    else if($url == '/ambienti'):
        $keyword = "";
        $description = "";
        $title = "Ristrutturazione Case";
        $author = "";
        $image = "";
    else if(strpos($url, '/ambienti/')):
        $keyword = "";
        $description = "";
        $title = "Ristrutturazione Case";
        $author = "";
        $image = "";
    else if($url == '/progetti'):
        $keyword = "";
        $description = "";
        $title = "Ristrutturazione Case";
        $author = "";
        $image = "";
    else if($url == '/contact'):
        $keyword = "";
        $description = "";
        $title = "Ristrutturazione Case";
        $author = "";
        $image = "";
    else if($url == '/il-tuo-architetto'):
        $keyword = "";
        $description = "";
        $title = "Ristrutturazione Case";
        $author = "";
        $image = "";
    else if($url == '/pdf'):
        $keyword = "";
        $description = "";
        $title = "Ristrutturazione Case";
        $author = "";
        $image = "";
    endif;


?>
<!DOCTYPE html>
<html lang="it">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="description" content="<?php echo $description;?>">
    <meta name="keywords" content="<?php echo $keywords;?>">
    <meta name="author" content="Simone Chinaglia">

    <meta property="og:title" content="<?php echo $title;?>" />
    <meta property="og:description" content="<?php echo $des;?>" />
    <meta property="og:url" content="<?php echo $title;?>" />
    <meta property="og:image" content="<?php echo $title;?>" />

    <link rel="stylesheet" href="/dev_assets/ristru/css/bootstrap.css">
    <link rel="stylesheet" href="/dev_assets/ristru/css/font-awesome.min.css">
    <link rel="stylesheet" href="/dev_assets/ristru/css/owl.theme.default.css">
    <link rel="stylesheet" href="/dev_assets/ristru/css/owl.carousel.css">
    <!--<link rel="stylesheet" href="https://dev2.simonechinaglia.net/dev_assets/ristru/fonts/stylesheet.css">-->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!--<link rel="stylesheet" href="https://dev2.simonechinaglia.net/dev/ristru/fonts/stylesheet.css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">-->
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700|Roboto:300,300i,400,400i,500,700,900&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
    <link rel="stylesheet" href="/dev_assets/ristru/css/main.css">
    <link rel="stylesheet" href="/dev_assets/ristru/css/overlap.css">

    <title><?php echo $title;?></title>
</head>
<body>
    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <div id="app"></div>
    <script src="/dist/build.js"></script>

    <script src="/dev_assets/ristru/js/vendor/jquery-1.11.2.min.js"></script>
    <script src="/dev_assets/ristru/js/vendor/bootstrap.js"></script>
    <script src="/dev_assets/ristru/js/owl.carousel.js"></script>

    <script>
        $(".cross_ic").click(function(){
           $(".header_top").hide();
        })
    </script>
</body>
</html>
